import matplotlib.pyplot as plt
import numpy             as np
import pandas            as pd
import scipy.fftpack     as scp_ff
import sys
from scipy  import signal

# Import function to create training and test set splits
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
# Import function to automatically create polynomial features! 
from sklearn.preprocessing    import PolynomialFeatures
# Import Linear Regression and a regularized regression function
from sklearn.linear_model     import LinearRegression
from sklearn.linear_model     import LassoCV
# Finally, import function to make a machine learning pipeline
from sklearn.pipeline         import make_pipeline
from sklearn.utils.validation import column_or_1d


# ----------------------------------------------------------------------------------------------------
# Fourier transform
# ----------------------------------------------------------------------------------------------------
def DFT_NAFF(h_orig,fft_h=np.array([]),n_near=-1,n_point=-1,prec_freq=1e-5,flg_ftr=False,flg_dbg=False):
  fft_data=pd.DataFrame()
  if (n_point<1):
    n_point=len(h_orig)                                 # Number of point
  #nit=np.ceil(np.log(n_point_i/2));                     # Number of cycle
  nit=int(np.ceil(-np.log(prec_freq*2.0*n_point)/np.log(20.0))); # Number of cycle
  # Filter
  # ----------------------------------------------------------------------------
  if (flg_ftr):
    h=h_orig*signal.hamming(n_point)
  else:
    h=h_orig
  if (nit>0):
    # Search for the nearest value of the tune
    # ----------------------------------------------------------------------------
    f    =np.arange(n_point)*1.0/n_point;
    #f    =scp_ff.fftfreq(n_point);
    if (len(fft_h)!=n_point):
      fft_h=scp_ff.fft(h);
    if (n_near<0):
      fft_data=pd.DataFrame({"Freq":f,"fft":fft_h,"abs_fft":abs(fft_h)});
      max_amp =fft_data.loc[2:(n_point/2)-1,"abs_fft"].max();
      n_near  =int(fft_data[fft_data["abs_fft"]==max_amp].index[0]);
    else:
      fft_data=pd.DataFrame({"Freq":f,"fft":fft_h,"abs_fft":abs(fft_h)});
    # Search for the tune using local dft (Case fast)
    # ----------------------------------------------------------------------------
    #   - initialize parameter
    n_low=n_near-1; n_hight=n_near+1;
    z  =np.exp(-1j*2.0*np.pi/n_point);
    F_m=np.zeros((21,n_point), dtype=complex);
    f_j=np.linspace(0,n_point-1,n_point);
    for it in range(0,nit):
      #   - create matrix for local fft
      f_i=np.linspace(n_low,n_hight,21);
      for i in range(0,21):
        for j in range(0,n_point):
         F_m[i,j]=z**(f_i[i]*f_j[j]);
      #   - compute fft
      fft_h=F_m.dot(h);
      #   - extract position of the tune
      fft_data_it=pd.DataFrame({"f_i":f_i,"fft":fft_h,"abs_fft":abs(fft_h)});
      max_amp_it =fft_data_it.loc[:,"abs_fft"].max();
      n          =int(fft_data_it[fft_data_it["abs_fft"]==max_amp_it].index[0]);
      #   - get nearest point
      if ((n>0) and (n<20)):
        n_low  =fft_data_it.loc[n-1,"f_i"];
        n_hight=fft_data_it.loc[n+1,"f_i"];
      elif (n==0 ):
        n_low  =2.0*fft_data_it.loc[0,"f_i"]-fft_data_it.loc[1,"f_i"];
        n_hight=fft_data_it.loc[1,"f_i"];
      elif (n==20):
        n_low  =fft_data_it.loc[19,"f_i"];
        n_hight=2.0*fft_data_it.loc[20,"f_i"]-fft_data_it.loc[19,"f_i"];
    # Save result
    # ----------------------------------------------------------------------------
    Q_res  =fft_data_it.loc[n,"f_i"    ]*1.0/n_point;
    max_amp=fft_data_it.loc[n,"abs_fft"];
    # Debug
    # ----------------------------------------------------------------------------
    if (flg_dbg):
#      print(h.dtype)
      if (n==0):
        print("DEBUG [Outils,DFT_NAFF]: n      =".format(n))
        print("DEBUG [Outils,DFT_NAFF]: n_low  =".format(n_low))
        print("DEBUG [Outils,DFT_NAFF]: n_hight=".format(n_hight))
        min_amp=fft_data.loc[0:(n_point/2)-1,"abs_fft"].min();
        # plot
        fig, ax = plt.subplots()
        fig, plt.plot(fft_data['Freq'],fft_data['abs_fft'],label="fft")
        fig, plt.plot(np.array([Q_res,Q_res]),np.array([min_amp,max_amp]),label="Q")
        fig, plt.plot(np.array([Q_res,Q_res]),np.array([min_amp,max_amp]),label="Q")
        fig, plt.yscale('log')
        fig, plt.title('DEBUG [Outils,DFT_NAFF]')
        ax.legend();
        plt.grid()
        plt.show()
        
      else:
        print("DEBUG [Outils,DFT_NAFF]: n={0}".format(n))
        Q_res_p=fft_data_it.loc[n+1,"f_i"    ]*1.0/n_point;
        Q_res_m=fft_data_it.loc[n-1,"f_i"    ]*1.0/n_point;
        max_amp_p=fft_data_it.loc[n+1,"abs_fft"];
        max_amp_m=fft_data_it.loc[n-1,"abs_fft"];
        min_amp=fft_data.loc[0:(n_point/2)-1,"abs_fft"].min();
        # plot
        fig, ax = plt.subplots()
        fig, plt.plot(fft_data['Freq'],fft_data['abs_fft'],label="fft")
        fig, plt.plot(np.array([Q_res,Q_res]),np.array([min_amp,max_amp]),label="Q")
        fig, plt.plot(np.array([Q_res_p,Q_res_p]),np.array([min_amp,max_amp_p]),label="Qn+1")
        fig, plt.plot(np.array([Q_res_m,Q_res_m]),np.array([min_amp,max_amp_m]),label="Qn-1")
        fig, plt.yscale('log')
        fig, plt.title('DEBUG [Outils,DFT_NAFF]')
        ax.legend();
        plt.grid()
        plt.show()
  else:
    print('WARNING [Outils,DFT_NAFF]: Precision asked should be lower than 1/n_point!')
    f    =np.arange(n_point)*1.0/n_point
    fft_h=scp_ff.fft(h)
    fft_data=pd.DataFrame({"Freq":f,"fft":fft_h,"abs_fft":abs(fft_h)})
    max_amp =fft_data.loc[0:(n_point/2)-1,"abs_fft"].max()
    n_near  =int(fft_data[fft_data["abs_fft"]==max_amp].index[0])
    print(n_near)
    Q_res   =fft_data.loc[n_near,"Freq"]
  return {'Q_res':Q_res, 'max_amp':max_amp,"fft":fft_data.loc[:,"fft"]}
    



def My_Fit(X,Y,deg,meth="polyfit",rge=[0,0]):
  if meth=="polyfit":
    return np.polyfit(X,Y,deg)
  elif meth=="train":  # https://towardsdatascience.com/machine-learning-with-python-easy-and-robust-method-to-fit-nonlinear-data-19e8a1ddbd49
    # Alpha (regularization strength) of LASSO regression
    lasso_eps = 0.0001
    lasso_nalpha=20
    lasso_iter=50000
    # Min and max degree of polynomials features to consider
    degree_min = 1
    degree_max = 4
    # Test/train split
    X_train, X_test, y_train, y_test = train_test_split(X['X'].values.reshape(-1, 1), Y['y'].values.reshape(-1, 1),test_size=0.2)
    
    # Make a pipeline model with polynomial transformation and LASSO regression with cross-validation, run it for increasing degree of polynomial (complexity of the model)
    #fig,  ax = plt.subplots()
    #fig2 = plt.figure()
    
    #fig, plt.plot(X['X'],Y['y'],'.')

    
    for degree in range(degree_min,degree_max+1):
      #model = make_pipeline(PolynomialFeatures(degree, interaction_only=False), LassoLarsCV(eps=lasso_eps,n_alphas=lasso_nalpha,max_iter=lasso_iter,normalize=True,cv=5))
      model = make_pipeline(PolynomialFeatures(degree, interaction_only=False), LassoCV(eps=lasso_eps,n_alphas=lasso_nalpha,max_iter=lasso_iter,normalize=True,cv=5))
      model.fit(X_train,y_train.ravel())
      test_pred = np.array(model.predict(X_test))
      RMSE=np.sqrt(np.sum(np.square(test_pred-y_test)))
      train_score = model.score(X_train,y_train)
      test_score  = model.score(X_test,y_test)


      #fig, plt.plot(X_test,test_pred,'.',label=degree)
      #fig2, plt.plot(degree,test_score,'.')
      

      print("d={0}  RMSE={1}  test_score={2}  train_score={2}".format(degree,RMSE,train_score))
      print(model.steps[1][1].coef_)
      print(model.steps[1][1].intercept_)
    #ax.legend();

    return np.asarray([0,0])
  else:
    KeyError('[Outils,My_Fit] meth='+meth)

def My_Pol(Coef,Y,meth="poly1d"):
  if meth=="poly1d":
    approx1=np.poly1d(Coef)
    return approx1(Y)
  else:
    KeyError('[Outils,My_Pol] meth='+meth)

