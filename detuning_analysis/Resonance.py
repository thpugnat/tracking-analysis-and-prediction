#!/usr/bin/env python3
import sys
from mpl_toolkits.mplot3d import Axes3D  
# Axes3D import has side effects, it enables using projection='3d' in add_subplot
import matplotlib.pyplot as plt
import pandas as pd
import numpy  as np


phi_x=0.25
phi_y=0.25

Q_x=60.31
Q_y=62.32

def fun(m,n):
    #return np.exp(2j*np.pi*(m*phi_x + n*phi_y))/(1-np.exp(2j*np.pi*(m*Q_x + n*Q_y)))
    #return np.exp(2j*np.pi*(m*phi_x + n*phi_y))/(1-np.exp(2j*np.pi*(m*Q_x + n*Q_y)))
    #return abs((np.exp(2j*np.pi*(m*phi_x + n*phi_y))/(1-np.exp(2j*np.pi*(m*Q_x + n*Q_y)))).real)
    return (np.exp(2j*np.pi*(m*phi_x + n*phi_y))/(1-np.exp(2j*np.pi*(m*Q_x + n*Q_y)))).real




fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#x = y = np.arange(-1.2, 1.2, 0.04)
x = y = np.arange(-0.22, 0.22, 0.004)
X, Y = np.meshgrid(x, y)
zs = np.array(fun(np.ravel(X), np.ravel(Y)))
Z = zs.reshape(X.shape)

ax.plot_surface(X, Y, Z)

ax.set_xlabel('m')
ax.set_ylabel('n')
ax.set_zlabel('Real(f)')
#ax.set_zscale('log')
ax.set_zlim(-5e-1,5e0)

plt.show()
