import matplotlib.pyplot as plt
import numpy  as np
import pandas as pd
import scipy.fftpack as scp_ff
from scipy  import signal
from cmath  import sqrt as csqrt
from Outils import DFT_NAFF

class particule:
    # Variable
    # ----------------------------------------------------------------------------------------------------
    idp     =0
    n_turn  =0
    Qx      =0.
    Qy      =0.
    twoJx_um=0.
    twoJy_um=0.
    twoJx_ummin=0.
    twoJy_ummin=0.
    twoJx_ummax=0.
    twoJy_ummax=0.
    #hdr=[]
    dtrack  =pd.DataFrame()
    fft     =pd.DataFrame()



    # ----------------------------------------------------------------------------------------------------
    # Initialization
    # ----------------------------------------------------------------------------------------------------
    def __init__(self, idp_tp=0, dt_tp=pd.DataFrame()):
      self.idp   = idp_tp
      self.n_turn= 0
      # prepare array for beam parameter
      self.twoJx_mu= 0.0;   self.twoJy_mu= 0.0;   self.Qx= 0.0;   self.Qy= 0.0
      self.fft=pd.DataFrame()
      #self.beam_param   = pd.DataFrame(np.array([[0, 0, 0, 0]]),columns=["2Jx[um]","2Jy[um]","Qx","Qy"])
      # Save data
      self.dtrack       = dt_tp.reset_index(drop=True)
      # Get number of turn
      if 'turn' in self.dtrack:
        self.n_turn=self.dtrack['turn'].max()
      # Get phase space
      if {'x[mm]','xp[mrad]','y[mm]','yp[mrad]'}.issubset(self.dtrack.columns):
        self.dtrack['hx-']= (self.dtrack['x[mm]']-1j*self.dtrack['xp[mrad]'])
        self.dtrack['hx+']= (self.dtrack['x[mm]']+1j*self.dtrack['xp[mrad]'])
        self.dtrack['hy-']= (self.dtrack['y[mm]']-1j*self.dtrack['yp[mrad]'])
        self.dtrack['hy+']= (self.dtrack['y[mm]']+1j*self.dtrack['yp[mrad]'])
        #self.hdr          = pd.Series( (v for v in self.dtrack.columns.tolist()) )
      #print(self.dtrack.count())
      #print(self.dtrack.dtypes)
     



    # ----------------------------------------------------------------------------------------------------
    # Overwrite operator
    # ----------------------------------------------------------------------------------------------------
    def __len__(self):
      return len(self.dtrack.index)

    def __getitem__(self, key):
      if   (key=="idp"):
        return self.idp
      elif (key=="n_turn"):
        return self.n_turn
      elif (key=="2Jx[um]" or key=="2Jx[$\mu$m]"):
        return self.twoJx_um
      elif (key=="2Jy[um]" or key=="2Jy[$\mu$m]"):
        return self.twoJy_um
      elif (key=="2Jx[um]min" or key=="2Jx[$\mu$m]min"):
        return self.twoJx_ummin
      elif (key=="2Jy[um]min" or key=="2Jy[$\mu$m]min"):
        return self.twoJy_ummin
      elif (key=="2Jx[um]max" or key=="2Jx[$\mu$m]max"):
        return self.twoJx_ummax
      elif (key=="2Jy[um]max" or key=="2Jy[$\mu$m]max"):
        return self.twoJy_ummax
      elif (key=="Qx"):
        return self.Qx
      elif (key=="Qy"):
        return self.Qy
      elif key in self.dtrack:
        return self.dtrack[key]
      elif key in self.fft:
        return self.fft[key]
      else:
        raise KeyError(key)

    #def __setitem__(self, key, value):
    #def __delitem__(self, key):
    #def __missing__(self, key):
    #def __contains__(self, item):
     



    # ----------------------------------------------------------------------------------------------------
    # Print element in class
    # ----------------------------------------------------------------------------------------------------
    def print_line(self, line, column=['']):
      #if np.count_nonzero(self.hdr.isin(column))>0:
      if 'column' in self.dtrack:
        print('Particule %5d: [%5d,%s]' % (self.idp,line,column))
        print(self.dtrack.loc[line,column])
      else:
        print('Particule %5d: [%5d,:]' % (self.idp,line))
        print(self.dtrack.loc[line,:])



    # ----------------------------------------------------------------------------------------------------
    # Plot element of the class
    # ----------------------------------------------------------------------------------------------------
    def plot_track(self,fig,ax,axis_x='x[mm]',axis_y='xp[mrad]'):
      if(self.dtrack.empty):
        raise TypeError('[class particule,plot_track]: No data!')
        #print('ERROR [class particule,fft]: No data!')
        #return
      else:
        mean_x=self.dtrack[axis_x].mean()
        mean_y=self.dtrack[axis_y].mean()
        lab="Part %d" % self.idp
        fig, plt.scatter(self.dtrack[axis_x],self.dtrack[axis_y],s=5 ,edgecolor='none',label=lab)
        fig, plt.scatter(mean_x,             mean_y,             s=10,edgecolor='none',marker='+',c="r")
        fig, plt.xlabel(axis_x)
        fig, plt.ylabel(axis_y)
        ax.legend();


    def plot_fft(self,fig,ax,axis='x'):
      if(self.fft.empty):
        raise TypeError('[class particule,plot_fft]: No data!')
        #print('ERROR [class particule,plot_fft]: No data')
        #return
      else:
        if ((axis=='x') or (axis=='all')):
          lab_hx1="hx-(%d)" % self.idp
          lab_hx2="hx+(%d)" % self.idp
          fig, plt.scatter(self.fft['freq'],abs(self.fft['ffhx-']),s=5,edgecolor='none',label=lab_hx1)
          fig, plt.scatter(self.fft['freq'],abs(self.fft['ffhx+']),s=5,edgecolor='none',label=lab_hx2)
          #fig, plt.yscale('log')
          fig, plt.xlabel('freq')
          fig, plt.ylabel('fft h')
          ax.legend();
        if ((axis=='y') or (axis=='all')):
          lab_hy1="hy-(%d)" % self.idp
          lab_hy2="hy+(%d)" % self.idp
          fig, plt.scatter(self.fft['freq'],abs(self.fft['ffhy-']),s=5,edgecolor='none',label=lab_hy1)
          fig, plt.scatter(self.fft['freq'],abs(self.fft['ffhy+']),s=5,edgecolor='none',label=lab_hy2)
          #fig, plt.yscale('log')
          fig, plt.xlabel('freq')
          fig, plt.ylabel('fft h')
          ax.legend();



    # ----------------------------------------------------------------------------------------------------
    # Function
    # ----------------------------------------------------------------------------------------------------
    def un_normalize(self,bx,ax,by,ay):
      if {'x[mm]','xp[mrad]','y[mm]','yp[mrad]'}.issubset(self.dtrack.columns):
        x=self.dtrack['x[mm]'].values; xp=self.dtrack['xp[mrad]'].values; sqrt_bx=csqrt(bx)
        y=self.dtrack['y[mm]'].values; yp=self.dtrack['yp[mrad]'].values; sqrt_by=csqrt(by)
        # Unnormalize the position
        self.dtrack['x[mm]'   ]=        x *sqrt_bx
        self.dtrack['xp[mrad]']= (xp-ax*x)/sqrt_bx
        self.dtrack['y[mm]'   ]=        y *sqrt_by
        self.dtrack['yp[mrad]']= (yp-ay*y)/sqrt_by
        # Get phase space
        self.dtrack['hx-']= (self.dtrack['x[mm]']-1j*self.dtrack['xp[mrad]'])
        self.dtrack['hx+']= (self.dtrack['x[mm]']+1j*self.dtrack['xp[mrad]'])
        self.dtrack['hy-']= (self.dtrack['y[mm]']-1j*self.dtrack['yp[mrad]'])
        self.dtrack['hy+']= (self.dtrack['y[mm]']+1j*self.dtrack['yp[mrad]'])
      



    def normalize(self,bx,ax,by,ay):
      if {'x[mm]','xp[mrad]','y[mm]','yp[mrad]'}.issubset(self.dtrack.columns):
        x=self.dtrack['x[mm]'].values; xp=self.dtrack['xp[mrad]'].values; sqrt_bx=csqrt(bx)
        y=self.dtrack['y[mm]'].values; yp=self.dtrack['yp[mrad]'].values; sqrt_by=csqrt(by)
        # Unnormalize the position
        self.dtrack['x[mm]'   ]= (              x /sqrt_bx )
        self.dtrack['xp[mrad]']= (xp*sqrt_bx+(ax/sqrt_bx)*x)#(xp+(ax/bx)*x)*sqrt_bx
        self.dtrack['y[mm]'   ]= (              y /sqrt_by )
        self.dtrack['yp[mrad]']= (yp*sqrt_by+(ay/sqrt_by)*y)#(yp+(ay/bx)*y)*sqrt_by
        # Get phase space
        self.dtrack['hx-']= (self.dtrack['x[mm]']-1j*self.dtrack['xp[mrad]'])
        self.dtrack['hx+']= (self.dtrack['x[mm]']+1j*self.dtrack['xp[mrad]'])
        self.dtrack['hy-']= (self.dtrack['y[mm]']-1j*self.dtrack['yp[mrad]'])
        self.dtrack['hy+']= (self.dtrack['y[mm]']+1j*self.dtrack['yp[mrad]'])



    # ----------------------------------------------------------------------------------------------------
    # Fourier transform
    # ----------------------------------------------------------------------------------------------------
    #def Detuning_fft(self, bx=121.566883016, ax=2.29573192543, by=218.584981915, ay=-2.64288996702, eps_f=1e-5,flg_ftr=False, flg_dbg=False):
    def Detuning_fft(self,eps_f=1e-5,flg_ftr=False,flg_dbg=False):
      if(self.dtrack.empty):
        raise TypeError('[class particule,fft]: No data!')
      elif (self.dtrack['turn'].max()>=1000):
        # Get action
        x  =self.dtrack.loc[0,'x[mm]'];     xp  =self.dtrack.loc[0,'xp[mrad]'];
        y  =self.dtrack.loc[0,'y[mm]'];     yp  =self.dtrack.loc[0,'yp[mrad]'];
        x_r=self.dtrack.loc[:,'x[mm]'];     xp_r=self.dtrack.loc[:,'xp[mrad]'];
        y_r=self.dtrack.loc[:,'y[mm]'];     yp_r=self.dtrack.loc[:,'yp[mrad]'];
        #self.twoJx_um=bx*(xp*xp) + (2.0*ax)*(x*xp) + ((1.0+ax*ax)/bx)*(x*x)
        #self.twoJy_um=by*(yp*yp) + (2.0*ay)*(y*yp) + ((1.0+ay*ay)/by)*(y*y)
        self.twoJx_um=       np.real((xp  *xp  ) + (x  *x  ))
        self.twoJy_um=       np.real((yp  *yp  ) + (y  *y  ))
        self.twoJx_ummin=min(np.real((xp_r*xp_r) + (x_r*x_r)))
        self.twoJy_ummin=min(np.real((yp_r*yp_r) + (y_r*y_r)))
        self.twoJx_ummax=max(np.real((xp_r*xp_r) + (x_r*x_r)))
        self.twoJy_ummax=max(np.real((yp_r*yp_r) + (y_r*y_r)))
        # Get Freq for fft
        tmp_f  =np.arange(self.n_turn)*1.0/self.n_turn
        # Get FFT and tune
        res=DFT_NAFF(self.dtrack['hx-'].values,    \
                     prec_freq=eps_f,              \
                     flg_ftr=flg_ftr,              \
                     flg_dbg=flg_dbg);
        self.Qx=res['Q_res']; tmp_hx1=res['fft'];
        res=DFT_NAFF(self.dtrack['hy-'].values,    \
                     prec_freq=eps_f,              \
                     flg_ftr=flg_ftr,              \
                     flg_dbg=flg_dbg);
        self.Qy=res['Q_res']; tmp_hy1=res['fft'];
        if(flg_ftr):
          hamming=signal.hamming(self.n_turn)
          #tmp_hx1=scp_ff.fft(self.dtrack['hx-'].values*hamming)
          #tmp_hy1=scp_ff.fft(self.dtrack['hy-'].values*hamming)
          tmp_hx2=scp_ff.fft(self.dtrack['hx+'].values*hamming)
          tmp_hy2=scp_ff.fft(self.dtrack['hy+'].values*hamming)
        else:
          #tmp_hx1=scp_ff.fft(self.dtrack['hx-'].values)
          #tmp_hy1=scp_ff.fft(self.dtrack['hy-'].values)
          tmp_hx2=scp_ff.fft(self.dtrack['hx+'].values)
          tmp_hy2=scp_ff.fft(self.dtrack['hy+'].values)
        self.fft=pd.DataFrame({'freq':tmp_f,                             \
                               'ffhx-':tmp_hx1,'ffhx+':tmp_hx2,          \
                               'ffhy-':tmp_hy1,'ffhy+':tmp_hy2})
        if(flg_dbg):
          print("DEBUG [class particule,fft]: ",self.idp, self.twoJx_um, self.Qx, self.twoJy_um, self.Qy)
        return
      else:
        print('WARNING [class particule,fft]: Not enougth point for part %d' % self.idp)
        return



    def Coupling_fft(self,ref_Qx,ref_Qy,axis_cf="x",wdw_sz=0.01,eps_f=1e-5,flg_ftr=False,flg_dbg=False):
      # Select phase-space to check
      if (axis_cf=="x"):
        hv=self.dtrack['hx-'].values;
        fft_hv=pd.DataFrame({'freq':self.fft['freq'].values,             \
                             'ffhv':self.fft['ffhx-'].values,            \
                             'abs_ffhv':abs(self.fft['ffhx-'].values)});
        ref_Qv=ref_Qx-np.floor(ref_Qx);
      elif (axis_cf=="y"):
        hv=self.dtrack['hy-'].values;
        fft_hv=pd.DataFrame({'freq':self.fft['freq'].values,             \
                             'ffhv':self.fft['ffhy-'].values,            \
                             'abs_ffhv':abs(self.fft['ffhy-'].values)});
        ref_Qv=ref_Qy-np.floor(ref_Qy);
      else:
        KeyError('[class particule,Coupling_fft]: '+axis_cf)
      # Select phase-space to check
      flg_f_window= abs(fft_hv['freq']-ref_Qv)<wdw_sz
      fft_hv_wdw  = fft_hv.loc[flg_f_window,:]
      max_ampv    = fft_hv_wdw['abs_ffhv'].max()
      im=int(fft_hv_wdw[fft_hv_wdw["abs_ffhv"]==max_ampv].index[0]);
      # Detect the interfece and count peaks
      negative = fft_hv_wdw['abs_ffhv'].values>max_ampv*0.75;
      len_wdw  = len(negative);
      interface= np.logical_xor(negative[0:len_wdw-1],negative[1:len_wdw]);
      interface_flag=0;
      # Detect if part of one peak is outside the domaine
# <<<<<<<<<<<<< debug
      #if (self.idp==59): #True):#
      #  print("DEBUG [class_particule, Coupling_fft] ref_Qv=%f" % ref_Qv)
      #  print("DEBUG [class_particule, Coupling_fft] wdw_sz=%f" % wdw_sz)
      #  print(np.sum(flg_f_window.values))
      #  print(len(fft_hv_wdw))
      #  print(fft_hv_wdw.columns.tolist())
      #  fig,ax=plt.subplots()
      #  fig, plt.plot(fft_hv['freq'],fft_hv['abs_ffhv'],label='full')
      #  fig, plt.plot(fft_hv_wdw['freq'],fft_hv_wdw['abs_ffhv'],label='wdw')
      #  fig, plt.legend()
      #  fig, plt.yscale('log')
      #  fig, plt.title('DEBUG [class_particule, Coupling_fft] (1) Part %d' % self.idp)
      #  plt.show()
# <<<<<<<<<<<<< debug
      if (not np.sum(interface)):
        return
      if (negative[0]):
        id_tp=np.array(range(len(interface)))[interface]
        interface[id_tp[ 0]]=False
        interface_flag=True
      if (negative[-1]):
        id_tp=np.array(range(len(interface)))[interface]
        interface[id_tp[-1]]=False
        interface_flag=True
      if (not np.sum(interface)):
        return
      # Detect if multipeak inside the domaine
      sum_interface=sum(interface);
      if (( sum_interface!=2 ) or (interface_flag)):
        inout=0;
        list_in=[]; list_ou=[];
        for it in range(len(interface)):
          if (interface[it]==1):
            if (inout==0):
              list_in.append(it);
              inout=1;
            else:
              list_ou.append(it+1);
              inout=0;
        dist=wdw_sz;
# <<<<<<<<<<<<< debug
        #if (self.idp==59): #True):#
        #print(range(list_in[0],list_ou[0]+1))
        #print(fft_hv_wdw[0].isin(range(list_in(0),list_ou(0)+1)))
        #fft_hv_wdw_loc1=fft_hv_wdw.loc[fft_hv_wdw[0].isin(range(list_in(0),list_ou(0)+1))]
        #print(fft_hv_wdw_loc1['abs_ffhv'])
        #fft_hv_wdw_loc2=fft_hv_wdw.loc[list_in(0):list_ou(0)]
        #print(fft_hv_wdw_loc2['abs_ffhv'])
        #print("DEBUG [class_particule, Coupling_fft] idp=%d" % self.idp)
# <<<<<<<<<<<<< debug
        mindex_fft_hv_wdw=fft_hv_wdw.index.min()
        for it in range(len(list_in)):
          #mask=((fft_hv_wdw.index.tolist()>=list_in[it]) and (fft_hv_wdw.index.tolist()<=list_ou[it]))
          mask=np.linspace(mindex_fft_hv_wdw+list_in[it],mindex_fft_hv_wdw+list_ou[it],list_ou[it]-list_in[it]+1,dtype=int).tolist()
          max_local=fft_hv_wdw.loc[mask,'abs_ffhv'].max()
          new_dist =abs(fft_hv_wdw.loc[fft_hv_wdw['abs_ffhv']==max_local,'freq'].values - ref_Qv);
          if (dist>new_dist):
            dist=new_dist;
            im=int(fft_hv_wdw[fft_hv_wdw["abs_ffhv"]==max_local].index[0]);
      # Final FFT to get the best precision for the tune
      res=DFT_NAFF(hv,fft_hv['ffhv'].values,n_near=im,prec_freq=eps_f,flg_ftr=flg_ftr,flg_dbg=flg_dbg)
      if (axis_cf=="x"):
        self.Qx=res['Q_res'];
      elif (axis_cf=="y"):
        self.Qy=res['Q_res'];
      # debug
      if (flg_dbg):
        if (( sum_interface!=2 ) or (interface_flag)):
          print("DEBUG [class_particule, Coupling_fft] Part.%d  - (sum_itrfc,itrfc_flag)=(%d,%r)" % self.idp,sum_interface,interface_flag)
        #print(np.c_[negative[0:len_wdw-1],negative[1:len_wdw],interface])
        #print(type(interface))
        fig,ax = plt.subplots()
        fig, plt.plot(fft_hv['freq'],    fft_hv['abs_ffhv'],    label='fft')
        fig, plt.plot(fft_hv_wdw['freq'],fft_hv_wdw['abs_ffhv'],label='fft wdw')
        fig, plt.plot([res['Q_res'],res['Q_res']],[fft_hv['abs_ffhv'].min(),fft_hv['abs_ffhv'].max()],label='Q_res')
        fig, plt.xlabel('f')
        fig, plt.ylabel('ffh'+axis_cf)
        fig, plt.yscale('log')
        fig, plt.title('DEBUG [class_particule, Coupling_fft] (2) Part %d' % self.idp)
        ax.legend();
        plt.grid()
        plt.show()
        

