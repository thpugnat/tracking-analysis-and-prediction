#!/usr/bin/env python3
import matplotlib.pyplot as plt
from class_case_Detuning_Simulation import Case_Detuning_Simulated as cas_DT_Sm
from matplotlib.pyplot import *


matplotlib.rcParams.update({'font.size': 16})
matplotlib.rc('xtick', labelsize=12) 
matplotlib.rc('ytick', labelsize=12) 

# Detuning Simulation
file=[]
file.append(cas_DT_Sm("nom MCOX OFF",case_col="g", \
            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_nominal/flatbeam_old/",   \
            file_type="ip3_Data_Part_s1_r01_Rat0_dlt0.txt"))
#file.append(cas_DT_Sm("Ricc MCOX OFF",case_col="r",  \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_riccardo/flatbeam_noMCOX/",  \
#            file_type="ip3_Data_Part_s1_r01_Rat0_dlt0.txt"))
#file.append(cas_DT_Sm("ND0 MCOX OFF",case_col="b",  \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_Lie2_ND0/flatbeam_noMCOX/",  \
#            file_type="ip3_Data_Part_C2_ND0_s1_r01_Rat0_dlt0.txt"))
#file.append(cas_DT_Sm("ND6 MCOX OFF",case_col="k", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_Lie2_ND6/flatbeam_noMCOX/",  \
#            file_type="ip3_Data_Part_C2_ND6_s1_r01_Rat0_dlt0.txt"))

#file.append(cas_DT_Sm("nom Sim",case_col="g", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_nominal/flatbeam/",   \
#            file_type="ip3_Data_Part_s1_r01_Rat0_dlt0_mctx0.txt"))
#file.append(cas_DT_Sm("Ricc Sim",case_col="r", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_riccardo/flatbeam/",  \
#            file_type="ip3_Data_Part_s1_r01_Rat0_dlt0_mctx0.txt"))
#file.append(cas_DT_Sm("ND0 Sim",case_col="b", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_Lie2_ND0/flatbeam/",  \
#            file_type="ip3_Data_Part_C2-6-10-14_ND0_s1_r01_Rat0_dlt0_mctx0.txt"))
#file.append(cas_DT_Sm("ND6 Sim",case_col="k",\
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_Lie2_ND6/flatbeam/",  \
#            file_type="ip3_Data_Part_C2-6-10-14_ND6_s1_r01_Rat0_dlt0_mctx0.txt"))
#file.append(cas_DT_Sm("nom mctx ON",case_col="g", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/track_nominal/flatbeam/",   \
#            file_type="ip3_Data_Part_s1_r01_Rat0_dlt0.txt"))
#file.append(cas_DT_Sm("noerr IR2-8",case_col="r", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/Test/",  \
#            file_type="ip3_Data_Part_v1.txt"))
#file.append(cas_DT_Sm("noerr IR2-8+Arc",case_col="b", \
#            path="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/Test/",  \
#            file_type="ip3_Data_Part_v2.txt"))

for f in file:
  # Set parameter
  f.set_refBPara(mx=0.31,my=0.32,                          \
                 bx=121.566883016135,ax=2.29573192543478,  \
                 by=218.584981914946,ay=-2.64288996702134)
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=False)

  # Get File to read containing the position
  f.getFile();

  # Read File and combine
  f.read_FilesandCombine();

  # fft
  f.analyse_Data()

  # refine fft
  f.refine_analyse_Data()



#  Fit Detuning
fig, ax = plt.subplots()
for f in file:
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
  f.fit_data(x_lab="2Jx[um]",y_lab="Qx",n_fit=2,meth=0,Q2=0)


#  Plot Detuning
fig, ax = plt.subplots()
for f in file:
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
  f.plot_DQ(fig,ax,x_lab_list="2Jx",y_lab_list="Qx",meth=0,Qref=0.31,Q2=0.0279,sigmaQ2=1e-3)
plt.show()


#fig, ax = plt.subplots()
#file[0].plot_DQ(fig,ax,x_lab_list="2Jx",y_lab_list="Qx",meth=0,Qref=0.31,Q2=0.0279,sigmaQ2=1e-3)
#plt.grid()
#plt.show()
#file[0].save_Data("DetwAmp_nominal_2JxumQx.txt",x_lab_list="2Jx[um]",y_lab_list="Qx")

#fig, ax = plt.subplots()
#file[1].plot_DQ(fig,ax,x_lab_list="2Jx",y_lab_list="Qx",meth=0,Qref=0.31,Q2=0.0279,sigmaQ2=1e-3)
#plt.grid()
#plt.show()
#file[1].save_Data('DetwAmp_noerrIR2-8_2JxumQx.txt',x_lab_list="2Jx[um]",y_lab_list="Qx")

#fig, ax = plt.subplots()
#file[2].plot_DQ(fig,ax,x_lab_list="2Jx",y_lab_list="Qx",meth=0,Qref=0.31,Q2=0.0279,sigmaQ2=1e-3)
#plt.grid()
#plt.show()
#file[2].save_Data('DetwAmp_noerrIR2-8Arcs_2JxumQx.txt',x_lab_list="2Jx[um]",y_lab_list="Qx")


#for f in file:
#  f.fit_data(x_lab="2Jx[um]",y_lab="Qy")



#files=function_getFile()
#data =function_read_FilesandCombine(files)
#print(data.dtypes)
#print(data[1].dtrack.loc[1:10,['hx+','hx-']])
#fig, ax = plt.subplots()
#file[0].data_part[0 ].plot_track(fig,ax,'x[mm]','xp[mrad]')
#file[0].data_part[19].plot_track(fig,ax,'x[mm]','xp[mrad]')
#file[0].data_part[39].plot_track(fig,ax,'x[mm]','xp[mrad]')
#file[0].data_part[59].plot_track(fig,ax,'x[mm]','xp[mrad]')
#ax.axis([-0.08, 0.08,-0.08, 0.08])
##ax.axis('equal')
##ax.set_aspect('equal', 'box')
#plt.grid()
#plt.show()

#data[0].fft()
#data[40].fft()
#d=data[0]['twoJx_um']
#print(d)
#for d in data:
#  d.fft()
#fig1, ax1 = plt.subplots()
#data[0].plot_fft(fig1,ax1,'x')
#data[40].plot_fft(fig1,ax1,'x')
#fig2, ax2 = plt.subplots()
#data[0].plot_fft(fig2,ax2,'y')
#data[40].plot_fft(fig2,ax2,'y')
#plt.grid()
#plt.show()


