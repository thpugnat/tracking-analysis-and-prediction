import matplotlib.pyplot as plt
#import pathlib
import numpy  as np
import pandas as pd
#import multiprocessing
#from multiprocessing import Pool
#from matplotlib.ticker import FormatStrFormatter



class file_FringeField:
    # Variable
    # --------------------------------------------------------------------------------------------------
    filename =""                 #
    Length_in=0                  #
    Position ='in'               # True=enter, False=exit
    Vec_Pot_r=pd.DataFrame()     #
    Vec_Pot_d=[]                 #
    dz       =0.                 # step in z
    length   =0.                 # length
    



    # --------------------------------------------------------------------------------------------------
    # Initialization
    # --------------------------------------------------------------------------------------------------
    def __init__(self,filename,Length_in,Position='in'):
      # Set input
      self.filename =filename
      self.Length_in=Length_in
      self.Position =Position
      self.Vec_Pot_r=pd.DataFrame()
      self.Vec_Pot_d=[]
      self.dz       =0.
      self.length   =0.




    def set(self,filename,Length_in,Position='in'):
      # Set input
      self.filename =filename
      self.Length_in=Length_in
      self.Position =Position
      self.Vec_Pot_r=pd.DataFrame()
      self.Vec_Pot_d=pd.DataFrame()
      self.dz       =0.
      self.length   =0.




    # --------------------------------------------------------------------------------------------------
    # Read File
    # --------------------------------------------------------------------------------------------------
    def load_File(self):
      if (self.filename==""):
        raise TypeError("[class file_FringeField,load_File]: wrong filename! Use set(filename,Length_in,Position)!")
      else:
        #    - read the data from the file
        self.Vec_Pot_r=pd.read_csv(self.filename,sep='\s+', names=['z','expx','expy','expz','C_Ax','C_Ay','C_Az']);
        if (not self.Vec_Pot_r.empty):
          z=self.Vec_Pot_r.z.unique()
          #      > get file info
          min_z      =min(z)
          self.length=max(z)-min_z
          self.dz    =self.length/(len(z)-1.)
          #      > Cut file by step
          vp_tp=[]
          for s in z:
            vp_sh=self.Vec_Pot_r[self.Vec_Pot_r["z"]==s]
            vp_tp.append(vp_sh)
          self.Vec_Pot_d=pd.DataFrame({"z":z-min_z, "data":vp_tp})
          #      > debug
          #print(z)
          #print(max(z))
          #print(min(z))
          #print(self.dz)




    # --------------------------------------------------------------------------------------------------
    # Compute detuning using Chao
    # --------------------------------------------------------------------------------------------------
    def DTune_Chao(self,f_BETX, f_BETY, f_ALFX, f_ALFY, norm):
      if (not self.Vec_Pot_r.empty):
        # Get KnL
        # ------------------------------------------------------------------------------
        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==0)
        Az3_KL_x = -4*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az3_S_x  = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_x=f_BETX(Az3_S_x)
        #Az3_bety_x=f_BETY(Az3_S_x)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==4)
        Az3_KL_y = -4*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az3_S_y  = self.Vec_Pot_r.loc[mask,'z'].values
        #Az3_betx_y=f_BETX(Az3_S_y)
        Az3_bety_y=f_BETY(Az3_S_y)
	
        #              * b5 + b3'' + b1''''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==6,self.Vec_Pot_r.expy.values==0)
        Az5_KL_x  = -6*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x=f_BETX(Az5_S_x)
        #Az5_bety_x=f_BETY(Az5_S_x)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==6)
        Az5_KL_y  = -6*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        #Az5_betx_y=f_BETX(Az5_S_y)
        Az5_bety_y=f_BETY(Az5_S_y)


        # Get AnL (odd derivatives)
        # ------------------------------------------------------------------------------
        #              * b1'
        mask=np.logical_and(self.Vec_Pot_r.expx.values==3,self.Vec_Pot_r.expy.values==0)
        Ax3_KL_x  = 4*self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax3_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax3_betx_x=f_BETX(Ax3_S_x)
        Ax3_alfx_x=f_ALFX(Ax3_S_x)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==3)
        Ay3_KL_y  = 4*self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay3_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay3_bety_y=f_BETY(Ay3_S_y)
        Ay3_alfy_y=f_ALFY(Ay3_S_y)
	
        #              * b3' + b1'''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==5,self.Vec_Pot_r.expy.values==0)
        Ax5_KL_x  = 6*self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax5_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_x=f_BETX(Ax5_S_x)
        Ax5_alfx_x=f_ALFX(Ax5_S_x)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==5)
        Ay5_KL_y  = 6*self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay5_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay5_bety_y=f_BETY(Ay5_S_y)
        Ay5_alfy_y=f_ALFY(Ay5_S_y)


        # Compute detuning coefficient
        # ------------------------------------------------------------------------------
        DQx=[];  DQy=[]
        DQx.append( [ 0, 0, 0] ) # (bety1   *K1Lx).sum()     /( 2.*2.*np.pi)])  #[]
        DQy.append( [ 0, 0, 0] ) # (betx2   *K1Ly).sum()     /( 2.*2.*np.pi)])  #[]
        DQx.append( [ 1, 0, ((Az3_betx_x**2*Az3_KL_x).sum()+((Ax3_betx_x*Ax3_alfx_x)*Ax3_KL_x).sum())*3e-6 /( 8.*(2.*np.pi))])  #[um-1]
        DQy.append( [ 0, 1, ((Az3_bety_y**2*Az3_KL_y).sum()+((Ay3_bety_y*Ay3_alfy_y)*Ay3_KL_y).sum())*3e-6 /( 8.*(2.*np.pi))])  #[um-1]
        DQx.append( [ 2, 0, ((Az5_betx_x**3*Az5_KL_x).sum()+((Ax5_betx_x*Ax5_alfx_x)*Ax5_KL_x).sum())*5e-12/(16.*(2.*np.pi))])  #[um-2]
        DQy.append( [ 0, 2, ((Az5_bety_y**3*Az5_KL_y).sum()+((Ay5_bety_y*Ay5_alfy_y)*Ay5_KL_y).sum())*5e-12/(16.*(2.*np.pi))])  #[um-2]
        DF_Qx=pd.DataFrame(DQx,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
        DF_Qy=pd.DataFrame(DQy,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])
        return DF_Qx,DF_Qy




    # --------------------------------------------------------------------------------------------------
    # ?????????????????????????????????
    # --------------------------------------------------------------------------------------------------
    def Compute_Cor(self,f_BETX, f_BETY,f_ALFX, f_ALFY, norm):
      res={"FFi_sum40":0,"FFi_sum04":0,"FFi_sum22":0,"FFi_sum60":0,"FFi_sum06":0,"FFi_sum42":0,"FFi_sum24":0}
      if (not self.Vec_Pot_r.empty):
        # Compute detuning coefficient
        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==0)
        Az3_KL_x  = -4*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm*6
        if len(Az3_KL_x)>0:
          Az3_KL_x[len(Az3_KL_x)-1]=0.
        Az3_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_x=f_BETX(Az3_S_x)
        Az3_bety_x=f_BETY(Az3_S_x)

        #mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==4)
        #Az3_KL_y  = -4*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm*6
        #if len(Az3_KL_y)>0:
        #  Az3_KL_y[len(Az3_KL_y)-1]=0.
        #Az3_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        #Az3_betx_y=f_BETX(Az3_S_y)
        #Az3_bety_y=f_BETY(Az3_S_y)

        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==6,self.Vec_Pot_r.expy.values==0)
        Az5_KL_x  = -6*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm*120
        if len(Az5_KL_x)>0:
          Az5_KL_x[len(Az5_KL_x)-1]=0.
        Az5_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x=f_BETX(Az5_S_x)
        Az5_bety_x=f_BETY(Az5_S_x)

        #mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==6)
        #Az5_KL_y  = -6*self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm*120
        #if len(Az5_KL_y)>0:
        #  Az5_KL_y[len(Az5_KL_y)-1]=0.
        #Az5_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        #Az5_betx_y=f_BETX(Az5_S_y)
        #Az5_bety_y=f_BETY(Az5_S_y)

        #              * b1'
        mask=np.logical_and(self.Vec_Pot_r.expx.values==3,self.Vec_Pot_r.expy.values==0)
        Ax3_KL_x  = 4*self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax3_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax3_betx_x=f_BETX(Ax3_S_x)
        Ax3_alfx_x=f_ALFX(Ax3_S_x)

        #mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==3)
        #Ay3_KL_y  = 4*self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        #Ay3_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        #Ay3_bety_y=f_BETY(Ay3_S_y)
        #Ay3_alfy_y=f_ALFY(Ay3_S_y)
	
        #              * b3' + b1'''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==5,self.Vec_Pot_r.expy.values==0)
        Ax5_KL_x  = 6*self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax5_S_x   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_x=f_BETX(Ax5_S_x)
        Ax5_alfx_x=f_ALFX(Ax5_S_x)

        #mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==5)
        #Ay5_KL_y  = 6*self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        #Ay5_S_y   = self.Vec_Pot_r.loc[mask,'z'].values
        #Ay5_bety_y=f_BETY(Ay5_S_y)
        #Ay5_alfy_y=f_ALFY(Ay5_S_y)

# <<<<<<<<<<< debug
#        Fig1,ax1=plt.subplots()
#        tp_in=self.Vec_Pot_r['z'].iloc[0];tp_ou=self.Vec_Pot_r['z'].iloc[-1];
#        tp=np.arange(tp_in,tp_ou,(tp_ou-tp_in)*.001)
#        Fig1, plt.plot(tp,f_BETX(tp),label="betx")
#        Fig1, plt.plot(tp,f_BETY(tp),label="bety")
#        ax1.legend()
#        plt.show()
# <<<<<<<<<<< debug

        #      - B3 and A3 corrector
        #FFi_sum12=0; 
        #FFi_sum21=0.; 

        #FFi_sum03=0.;
        #FFi_sum30=0.;
        #      - B4 and A4 corrector
        FFi_sum40=( Az3_betx_x**2               *Az3_KL_x).sum()+((Ax3_betx_x   *Ax3_alfx_x)*Ax3_KL_x).sum(); 
        FFi_sum04=(               Az3_bety_x**2 *Az3_KL_x).sum()+((Ax3_bety_x   *Ax3_alfy_x)*Ax3_KL_x).sum();
        FFi_sum22=((Az3_betx_x   *Az3_bety_x   )*Az3_KL_x).sum()+((Ax3_bety_x*Ax3_alfx_x-Ax3_betx_x*Ax3_alfy_x)*Ax3_KL_x).sum(); 

        #FFi_sum31=0.; 
        #FFi_sum13=0.;
        #      - B5 and A5 corrector
        #FFi_sum50=0.;
        #FFi_sum05=0.;

        #FFi_sum32=0.;
        #FFi_sum14=0.;

        #FFi_sum41=0.;
        #FFi_sum23=0.;
        #      - B6 and A6 corrector
        FFi_sum60=( Az5_betx_x**3                *Az5_KL_x).sum()+((Ax5_betx_x**2*Ax5_alfx_x)*Ax5_KL_x).sum();
        FFi_sum06=(                Az5_bety_x**3 *Az5_KL_x).sum()+((Ax5_bety_x**2*Ax5_alfy_x)*Ax5_KL_x).sum();
        FFi_sum42=((Az5_betx_x**2 *Az5_bety_x   )*Az5_KL_x).sum()+((Az5_betx_x*(Ax5_bety_x*Ax5_alfx_x-Ax5_betx_x*Ax5_alfy_x))*Ax5_KL_x).sum();
        FFi_sum24=((Az5_betx_x    *Az5_bety_x**2)*Az5_KL_x).sum()+((Az5_bety_x*(Ax5_bety_x*Ax5_alfx_x-Ax5_betx_x*Ax5_alfy_x))*Ax5_KL_x).sum();

        # Compute detuning coefficient
        res={"FFi_sum40":FFi_sum40,"FFi_sum04":FFi_sum04,"FFi_sum22":FFi_sum22,
             "FFi_sum60":FFi_sum60,"FFi_sum06":FFi_sum06,"FFi_sum42":FFi_sum42,"FFi_sum24":FFi_sum24}
      return res





    # --------------------------------------------------------------------------------------------------
    # Plot data
    # --------------------------------------------------------------------------------------------------
    def plot_vecpot(self,fig,ax,axis,rx=0.1,ry=0.):
      if (self.Vec_Pot_r.empty):
        raise TypeError("[class file_FringeField,plot_vecpot]: No data!")
      else:
        #    - read the data from the file
        tab=[]
        for i in range(len(self.Vec_Pot_d)):
          tp=self.Vec_Pot_d.loc[i,'data']
          t =sum((rx**tp['expx'])*(ry**tp['expy'])*tp['C_'+axis])
          tab.append(t)
        fig, plt.plot(self.Vec_Pot_d["z"],tab,"-.",label=axis)
        ax.legend();
        plt.grid()
