#!/usr/bin/env python3
#import sys
#import matplotlib.pyplot as plt
#from matplotlib.pyplot import *


#sys.path.insert(0, './detuning_prediction/')
from class_case_Detuning_Theoric    import case_Detuning_Theoric   as cas_DT_Th


#matplotlib.rcParams.update({'font.size': 14})
#matplotlib.rc('xtick', labelsize=14) 
#matplotlib.rc('ytick', labelsize=14) 





# Data analytic
# -----------------------------------------------------------------------------------
Analytic1=[]
drt="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#         * nominal
#             > Define case
c=cas_DT_Th(case_name="HE Analytic",case_col="g", \
            path_opt=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly/onlytriper_b4eq0/", \
            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly/onlytriper_b4eq0/temp_1/")
#             > Save case
Analytic1.append(c)

#         * Riccardo
#             > Define case
c=cas_DT_Th(case_name="HE+Heads Analytic",case_col="r", \
            path_opt=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/", \
            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/onlytrip_nob4/temp_1/")
#             > Save case
Analytic1.append(c)

#         * ND0
#             > Define case
c=cas_DT_Th(case_name="Lie2 ND0 Analytic",case_col="b", \
            path_opt=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/", \
            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/")
#             > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,      5,       1)
c.add_Quad("MQXFA.A1L5..16",      10,      0)
c.add_Quad("MQXFA.B1L5..1" ,      4  ,     1)
c.add_Quad("MQXFA.B1L5..16",      12,      0)
c.add_Quad("MQXFB.A2L5..1" ,      3 ,      1)
c.add_Quad("MQXFB.A2L5..16",      8 ,      0)
c.add_Quad("MQXFB.B2L5..1" ,      1 ,      1)
c.add_Quad("MQXFB.B2L5..16",      7 ,      0)
c.add_Quad("MQXFA.A3L5..1" ,      5 ,      1)
c.add_Quad("MQXFA.A3L5..16",      10,      0)
c.add_Quad("MQXFA.B3L5..1" ,      4 ,      1)
c.add_Quad("MQXFA.B3L5..16",      12,      0)
c.add_Quad("MQXFA.A3R5..1" ,      3 ,      1)
c.add_Quad("MQXFA.A3R5..16",      9 ,      0)
c.add_Quad("MQXFA.B3R5..1" ,      2 ,      1)
c.add_Quad("MQXFA.B3R5..16",      7 ,      0)
c.add_Quad("MQXFB.A2R5..1" ,      6 ,      1)
c.add_Quad("MQXFB.A2R5..16",      10,      0)
c.add_Quad("MQXFB.B2R5..1" ,      4 ,      1)
c.add_Quad("MQXFB.B2R5..16",      11,      0)
c.add_Quad("MQXFA.A1R5..1" ,      3,       1)
c.add_Quad("MQXFA.A1R5..16",      9,       0)
c.add_Quad("MQXFA.B1R5..1" ,      2,       1)
c.add_Quad("MQXFA.B1R5..16",      7,       0)
c.add_Quad("MQXFA.A1L1..1" ,      5,       1)
c.add_Quad("MQXFA.A1L1..16",      10,      0)
c.add_Quad("MQXFA.B1L1..1" ,      4  ,     1)
c.add_Quad("MQXFA.B1L1..16",      12,      0)
c.add_Quad("MQXFB.A2L1..1" ,      3 ,      1)
c.add_Quad("MQXFB.A2L1..16",      8 ,      0)
c.add_Quad("MQXFB.B2L1..1" ,      1 ,      1)
c.add_Quad("MQXFB.B2L1..16",      7 ,      0)
c.add_Quad("MQXFA.A3L1..1" ,      5 ,      1)
c.add_Quad("MQXFA.A3L1..16",      10,      0)
c.add_Quad("MQXFA.B3L1..1" ,      4 ,      1)
c.add_Quad("MQXFA.B3L1..16",      12,      0)
c.add_Quad("MQXFA.A3R1..1" ,      3 ,      1)
c.add_Quad("MQXFA.A3R1..16",      9 ,      0)
c.add_Quad("MQXFA.B3R1..1" ,      2 ,      1)
c.add_Quad("MQXFA.B3R1..16",      7 ,      0)
c.add_Quad("MQXFB.A2R1..1" ,      6 ,      1)
c.add_Quad("MQXFB.A2R1..16",      10,      0)
c.add_Quad("MQXFB.B2R1..1" ,      4 ,      1)
c.add_Quad("MQXFB.B2R1..16",      11,      0)
c.add_Quad("MQXFA.A1R1..1" ,      3,       1)
c.add_Quad("MQXFA.A1R1..16",      9,       0)
c.add_Quad("MQXFA.B1R1..1" ,      2,       1)
c.add_Quad("MQXFA.B1R1..16",      7,       0)
#             > Add Skip
c.add_Skip("MQXFA.B3L5..FL")
c.add_Skip("MQXFA.A3L5..FL")
c.add_Skip("MQXFB.B2L5..FL")
c.add_Skip("MQXFB.A2L5..FL")
c.add_Skip("MQXFA.B1L5..FL")
c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL")
c.add_Skip("MQXFA.B1R5..FL")
c.add_Skip("MQXFB.A2R5..FL")
c.add_Skip("MQXFB.B2R5..FL")
c.add_Skip("MQXFA.A3R5..FL")
c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL")
c.add_Skip("MQXFA.A3L1..FL")
c.add_Skip("MQXFB.B2L1..FL")
c.add_Skip("MQXFB.A2L1..FL")
c.add_Skip("MQXFA.B1L1..FL")
c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL")
c.add_Skip("MQXFA.B1R1..FL")
c.add_Skip("MQXFB.A2R1..FL")
c.add_Skip("MQXFB.B2R1..FL")
c.add_Skip("MQXFA.A3R1..FL")
c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR")
c.add_Skip("MQXFA.A3L5..FR")
c.add_Skip("MQXFB.B2L5..FR")
c.add_Skip("MQXFB.A2L5..FR")
c.add_Skip("MQXFA.B1L5..FR")
c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR")
c.add_Skip("MQXFA.B1R5..FR")
c.add_Skip("MQXFB.A2R5..FR")
c.add_Skip("MQXFB.B2R5..FR")
c.add_Skip("MQXFA.A3R5..FR")
c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR")
c.add_Skip("MQXFA.A3L1..FR")
c.add_Skip("MQXFB.B2L1..FR")
c.add_Skip("MQXFB.A2L1..FR")
c.add_Skip("MQXFA.B1L1..FR")
c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR")
c.add_Skip("MQXFA.B1R1..FR")
c.add_Skip("MQXFB.A2R1..FR")
c.add_Skip("MQXFB.B2R1..FR")
c.add_Skip("MQXFA.A3R1..FR")
c.add_Skip("MQXFA.B3R1..FR")
#             > Add File
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_in_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_HFC/coeff_out_HFC_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#             > Save case
#Analytic1.append(c)









# Compute detuning from analytic data
# --------------------------------------------------------------------------------
for t in Analytic1:
  t.load_File()
  t.load_FFFile()
#  t.DTune_Andr()
#  t.DTune_Chao()
  t.Compute_Cor()
  print(t.KCor)























# 
